namespace PR
{
    internal class PRInfo
    {
        public string RemoteName { get; set; }
        public string RemoteUrl { get; set; }
        public string BranchName { get; set; }
    }
}